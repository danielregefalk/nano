/root/nano/nano
Using TensorFlow backend.
Weights:  coco
Dataset:  data
Logs:  /root/nano/logs

Configurations:
AUGMENT                        True
BACKBONE                       resnet101
BACKBONE_STRIDES               [4, 8, 16, 32, 64]
BATCH_SIZE                     2
BBOX_STD_DEV                   [0.1 0.1 0.2 0.2]
COMPUTE_BACKBONE_SHAPE         None
DETECTION_MAX_INSTANCES        100
DETECTION_MIN_CONFIDENCE       0
DETECTION_NMS_THRESHOLD        0.3
FPN_CLASSIF_FC_LAYERS_SIZE     1024
GPU_COUNT                      1
GRADIENT_CLIP_NORM             5.0
IMAGES_PER_GPU                 2
IMAGE_CHANNEL_COUNT            3
IMAGE_MAX_DIM                  1024
IMAGE_META_SIZE                14
IMAGE_MIN_DIM                  800
IMAGE_MIN_SCALE                0
IMAGE_RESIZE_MODE              square
IMAGE_SHAPE                    [1024 1024    3]
LEARNING_MOMENTUM              0.9
LEARNING_RATE                  0.001
LOSS_WEIGHTS                   {'rpn_class_loss': 1.0, 'rpn_bbox_loss': 1.0, 'mrcnn_class_loss': 1.0, 'mrcnn_bbox_loss': 1.0, 'mrcnn_mask_loss': 1.0}
MASK_POOL_SIZE                 14
MASK_SHAPE                     [28, 28]
MAX_GT_INSTANCES               50
MEAN_PIXEL                     [123.7 116.8 103.9]
MINI_MASK_SHAPE                (56, 56)
NAME                           nano
NUM_CLASSES                    2
OPTIMIZER                      ADAM
POOL_SIZE                      7
POST_NMS_ROIS_INFERENCE        2000
POST_NMS_ROIS_TRAINING         2000
PRE_NMS_LIMIT                  6000
ROI_POSITIVE_RATIO             0.33
RPN_ANCHOR_RATIOS              [0.5, 1, 2]
RPN_ANCHOR_SCALES              (32, 64, 128, 256, 512)
RPN_ANCHOR_STRIDE              1
RPN_BBOX_STD_DEV               [0.1 0.1 0.2 0.2]
RPN_NMS_THRESHOLD              0.9
RPN_TRAIN_ANCHORS_PER_IMAGE    64
SCALE                          True
STEPS_PER_EPOCH                300
TOP_DOWN_PYRAMID_SIZE          256
TRAIN_BN                       False
TRAIN_ROIS_PER_IMAGE           200
USE_MINI_MASK                  True
USE_RPN_ROIS                   True
VALIDATION_STEPS               50
WEIGHT_DECAY                   0.0001


WARNING:tensorflow:From /usr/local/lib/python3.6/dist-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.
Instructions for updating:
Colocations handled automatically by placer.
Downloading pretrained model to /root/nano/mask_rcnn_coco.h5 ...
... done downloading pretrained model!
Loading weights  /root/nano/mask_rcnn_coco.h5
2019-04-09 06:37:29.150336: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2299995000 Hz
2019-04-09 06:37:29.150860: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x18cca520 executing computations on platform Host. Devices:
2019-04-09 06:37:29.150907: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): <undefined>, <undefined>
2019-04-09 06:37:29.313455: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:998] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2019-04-09 06:37:29.314101: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x18cca940 executing computations on platform CUDA. Devices:
2019-04-09 06:37:29.314148: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): Tesla K80, Compute Capability 3.7
2019-04-09 06:37:29.314565: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1433] Found device 0 with properties: 
name: Tesla K80 major: 3 minor: 7 memoryClockRate(GHz): 0.8235
pciBusID: 0000:00:04.0
totalMemory: 11.17GiB freeMemory: 11.10GiB
2019-04-09 06:37:29.314602: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1512] Adding visible gpu devices: 0
2019-04-09 06:37:30.845705: I tensorflow/core/common_runtime/gpu/gpu_device.cc:984] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-04-09 06:37:30.845802: I tensorflow/core/common_runtime/gpu/gpu_device.cc:990]      0 
2019-04-09 06:37:30.845825: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1003] 0:   N 
2019-04-09 06:37:30.846116: W tensorflow/core/common_runtime/gpu/gpu_bfc_allocator.cc:42] Overriding allow_growth setting because the TF_FORCE_GPU_ALLOW_GROWTH environment variable is set. Original config value was 0.
2019-04-09 06:37:30.846239: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1115] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10754 MB memory) -> physical GPU (device: 0, name: Tesla K80, pci bus id: 0000:00:04.0, compute capability: 3.7)
Authentication google drive
Go to the following link in your browser:

    https://accounts.google.com/o/oauth2/auth?redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&prompt=select_account&response_type=code&client_id=32555940559.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcloud-platform+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fappengine.admin+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcompute+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive&access_type=offline

Enter verification code: 
Training network heads

Starting at epoch 0. LR=0.001

Checkpoint Path: /root/nano/logs/nano20190409T0637/mask_rcnn_nano_{epoch:04d}.h5
Selecting layers to train
conv1                  (Conv2D)
bn_conv1               (BatchNorm)
res2a_branch2a         (Conv2D)
bn2a_branch2a          (BatchNorm)
res2a_branch2b         (Conv2D)
bn2a_branch2b          (BatchNorm)
res2a_branch2c         (Conv2D)
res2a_branch1          (Conv2D)
bn2a_branch2c          (BatchNorm)
bn2a_branch1           (BatchNorm)
res2b_branch2a         (Conv2D)
bn2b_branch2a          (BatchNorm)
res2b_branch2b         (Conv2D)
bn2b_branch2b          (BatchNorm)
res2b_branch2c         (Conv2D)
bn2b_branch2c          (BatchNorm)
res2c_branch2a         (Conv2D)
bn2c_branch2a          (BatchNorm)
res2c_branch2b         (Conv2D)
bn2c_branch2b          (BatchNorm)
res2c_branch2c         (Conv2D)
bn2c_branch2c          (BatchNorm)
res3a_branch2a         (Conv2D)
bn3a_branch2a          (BatchNorm)
res3a_branch2b         (Conv2D)
bn3a_branch2b          (BatchNorm)
res3a_branch2c         (Conv2D)
res3a_branch1          (Conv2D)
bn3a_branch2c          (BatchNorm)
bn3a_branch1           (BatchNorm)
res3b_branch2a         (Conv2D)
bn3b_branch2a          (BatchNorm)
res3b_branch2b         (Conv2D)
bn3b_branch2b          (BatchNorm)
res3b_branch2c         (Conv2D)
bn3b_branch2c          (BatchNorm)
res3c_branch2a         (Conv2D)
bn3c_branch2a          (BatchNorm)
res3c_branch2b         (Conv2D)
bn3c_branch2b          (BatchNorm)
res3c_branch2c         (Conv2D)
bn3c_branch2c          (BatchNorm)
res3d_branch2a         (Conv2D)
bn3d_branch2a          (BatchNorm)
res3d_branch2b         (Conv2D)
bn3d_branch2b          (BatchNorm)
res3d_branch2c         (Conv2D)
bn3d_branch2c          (BatchNorm)
res4a_branch2a         (Conv2D)
bn4a_branch2a          (BatchNorm)
res4a_branch2b         (Conv2D)
bn4a_branch2b          (BatchNorm)
res4a_branch2c         (Conv2D)
res4a_branch1          (Conv2D)
bn4a_branch2c          (BatchNorm)
bn4a_branch1           (BatchNorm)
res4b_branch2a         (Conv2D)
bn4b_branch2a          (BatchNorm)
res4b_branch2b         (Conv2D)
bn4b_branch2b          (BatchNorm)
res4b_branch2c         (Conv2D)
bn4b_branch2c          (BatchNorm)
res4c_branch2a         (Conv2D)
bn4c_branch2a          (BatchNorm)
res4c_branch2b         (Conv2D)
bn4c_branch2b          (BatchNorm)
res4c_branch2c         (Conv2D)
bn4c_branch2c          (BatchNorm)
res4d_branch2a         (Conv2D)
bn4d_branch2a          (BatchNorm)
res4d_branch2b         (Conv2D)
bn4d_branch2b          (BatchNorm)
res4d_branch2c         (Conv2D)
bn4d_branch2c          (BatchNorm)
res4e_branch2a         (Conv2D)
bn4e_branch2a          (BatchNorm)
res4e_branch2b         (Conv2D)
bn4e_branch2b          (BatchNorm)
res4e_branch2c         (Conv2D)
bn4e_branch2c          (BatchNorm)
res4f_branch2a         (Conv2D)
bn4f_branch2a          (BatchNorm)
res4f_branch2b         (Conv2D)
bn4f_branch2b          (BatchNorm)
res4f_branch2c         (Conv2D)
bn4f_branch2c          (BatchNorm)
res4g_branch2a         (Conv2D)
bn4g_branch2a          (BatchNorm)
res4g_branch2b         (Conv2D)
bn4g_branch2b          (BatchNorm)
res4g_branch2c         (Conv2D)
bn4g_branch2c          (BatchNorm)
res4h_branch2a         (Conv2D)
bn4h_branch2a          (BatchNorm)
res4h_branch2b         (Conv2D)
bn4h_branch2b          (BatchNorm)
res4h_branch2c         (Conv2D)
bn4h_branch2c          (BatchNorm)
res4i_branch2a         (Conv2D)
bn4i_branch2a          (BatchNorm)
res4i_branch2b         (Conv2D)
bn4i_branch2b          (BatchNorm)
res4i_branch2c         (Conv2D)
bn4i_branch2c          (BatchNorm)
res4j_branch2a         (Conv2D)
bn4j_branch2a          (BatchNorm)
res4j_branch2b         (Conv2D)
bn4j_branch2b          (BatchNorm)
res4j_branch2c         (Conv2D)
bn4j_branch2c          (BatchNorm)
res4k_branch2a         (Conv2D)
bn4k_branch2a          (BatchNorm)
res4k_branch2b         (Conv2D)
bn4k_branch2b          (BatchNorm)
res4k_branch2c         (Conv2D)
bn4k_branch2c          (BatchNorm)
res4l_branch2a         (Conv2D)
bn4l_branch2a          (BatchNorm)
res4l_branch2b         (Conv2D)
bn4l_branch2b          (BatchNorm)
res4l_branch2c         (Conv2D)
bn4l_branch2c          (BatchNorm)
res4m_branch2a         (Conv2D)
bn4m_branch2a          (BatchNorm)
res4m_branch2b         (Conv2D)
bn4m_branch2b          (BatchNorm)
res4m_branch2c         (Conv2D)
bn4m_branch2c          (BatchNorm)
res4n_branch2a         (Conv2D)
bn4n_branch2a          (BatchNorm)
res4n_branch2b         (Conv2D)
bn4n_branch2b          (BatchNorm)
res4n_branch2c         (Conv2D)
bn4n_branch2c          (BatchNorm)
res4o_branch2a         (Conv2D)
bn4o_branch2a          (BatchNorm)
res4o_branch2b         (Conv2D)
bn4o_branch2b          (BatchNorm)
res4o_branch2c         (Conv2D)
bn4o_branch2c          (BatchNorm)
res4p_branch2a         (Conv2D)
bn4p_branch2a          (BatchNorm)
res4p_branch2b         (Conv2D)
bn4p_branch2b          (BatchNorm)
res4p_branch2c         (Conv2D)
bn4p_branch2c          (BatchNorm)
res4q_branch2a         (Conv2D)
bn4q_branch2a          (BatchNorm)
res4q_branch2b         (Conv2D)
bn4q_branch2b          (BatchNorm)
res4q_branch2c         (Conv2D)
bn4q_branch2c          (BatchNorm)
res4r_branch2a         (Conv2D)
bn4r_branch2a          (BatchNorm)
res4r_branch2b         (Conv2D)
bn4r_branch2b          (BatchNorm)
res4r_branch2c         (Conv2D)
bn4r_branch2c          (BatchNorm)
res4s_branch2a         (Conv2D)
bn4s_branch2a          (BatchNorm)
res4s_branch2b         (Conv2D)
bn4s_branch2b          (BatchNorm)
res4s_branch2c         (Conv2D)
bn4s_branch2c          (BatchNorm)
res4t_branch2a         (Conv2D)
bn4t_branch2a          (BatchNorm)
res4t_branch2b         (Conv2D)
bn4t_branch2b          (BatchNorm)
res4t_branch2c         (Conv2D)
bn4t_branch2c          (BatchNorm)
res4u_branch2a         (Conv2D)
bn4u_branch2a          (BatchNorm)
res4u_branch2b         (Conv2D)
bn4u_branch2b          (BatchNorm)
res4u_branch2c         (Conv2D)
bn4u_branch2c          (BatchNorm)
res4v_branch2a         (Conv2D)
bn4v_branch2a          (BatchNorm)
res4v_branch2b         (Conv2D)
bn4v_branch2b          (BatchNorm)
res4v_branch2c         (Conv2D)
bn4v_branch2c          (BatchNorm)
res4w_branch2a         (Conv2D)
bn4w_branch2a          (BatchNorm)
res4w_branch2b         (Conv2D)
bn4w_branch2b          (BatchNorm)
res4w_branch2c         (Conv2D)
bn4w_branch2c          (BatchNorm)
res5a_branch2a         (Conv2D)
bn5a_branch2a          (BatchNorm)
res5a_branch2b         (Conv2D)
bn5a_branch2b          (BatchNorm)
res5a_branch2c         (Conv2D)
res5a_branch1          (Conv2D)
bn5a_branch2c          (BatchNorm)
bn5a_branch1           (BatchNorm)
res5b_branch2a         (Conv2D)
bn5b_branch2a          (BatchNorm)
res5b_branch2b         (Conv2D)
bn5b_branch2b          (BatchNorm)
res5b_branch2c         (Conv2D)
bn5b_branch2c          (BatchNorm)
res5c_branch2a         (Conv2D)
bn5c_branch2a          (BatchNorm)
res5c_branch2b         (Conv2D)
bn5c_branch2b          (BatchNorm)
res5c_branch2c         (Conv2D)
bn5c_branch2c          (BatchNorm)
fpn_c5p5               (Conv2D)
fpn_c4p4               (Conv2D)
fpn_c3p3               (Conv2D)
fpn_c2p2               (Conv2D)
fpn_p5                 (Conv2D)
fpn_p2                 (Conv2D)
fpn_p3                 (Conv2D)
fpn_p4                 (Conv2D)
In model:  rpn_model
    rpn_conv_shared        (Conv2D)
    rpn_class_raw          (Conv2D)
    rpn_bbox_pred          (Conv2D)
mrcnn_mask_conv1       (TimeDistributed)
mrcnn_mask_bn1         (TimeDistributed)
mrcnn_mask_conv2       (TimeDistributed)
mrcnn_mask_bn2         (TimeDistributed)
mrcnn_class_conv1      (TimeDistributed)
mrcnn_class_bn1        (TimeDistributed)
mrcnn_mask_conv3       (TimeDistributed)
mrcnn_mask_bn3         (TimeDistributed)
mrcnn_class_conv2      (TimeDistributed)
mrcnn_class_bn2        (TimeDistributed)
mrcnn_mask_conv4       (TimeDistributed)
mrcnn_mask_bn4         (TimeDistributed)
mrcnn_bbox_fc          (TimeDistributed)
mrcnn_mask_deconv      (TimeDistributed)
mrcnn_class_logits     (TimeDistributed)
mrcnn_mask             (TimeDistributed)
WARNING:tensorflow:From /usr/local/lib/python3.6/dist-packages/tensorflow/python/ops/math_ops.py:3066: to_int32 (from tensorflow.python.ops.math_ops) is deprecated and will be removed in a future version.
Instructions for updating:
Use tf.cast instead.
/usr/local/lib/python3.6/dist-packages/tensorflow/python/ops/gradients_impl.py:110: UserWarning: Converting sparse IndexedSlices to a dense Tensor of unknown shape. This may consume a large amount of memory.
  "Converting sparse IndexedSlices to a dense Tensor of unknown shape. "
/usr/local/lib/python3.6/dist-packages/keras/engine/training_generator.py:47: UserWarning: Using a generator with `use_multiprocessing=True` and multiple workers may duplicate your data. Please consider using the`keras.utils.Sequence class.
  UserWarning('Using a generator with `use_multiprocessing=True`'
Epoch 1/20
2019-04-09 06:39:40.221372: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:39:40.221547: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice_37. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:40:23.815883: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:40:23.816024: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice_37. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:40:34.032023: I tensorflow/stream_executor/dso_loader.cc:152] successfully opened CUDA library libcublas.so.10.0 locally
2019-04-09 06:40:49.111041: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.53GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:50.131080: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.14GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:50.186278: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.13GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:50.608828: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 2.25GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:50.742017: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.13GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:51.786177: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 1.10GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:53.742603: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 1.49GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:53.742721: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.14GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:54.328935: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 3.03GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
2019-04-09 06:40:54.335839: W tensorflow/core/common_runtime/bfc_allocator.cc:211] Allocator (GPU_0_bfc) ran out of memory trying to allocate 1.10GiB. The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
299/300 [============================>.] - ETA: 4s - loss: 0.6354 - rpn_class_loss: 0.0347 - rpn_bbox_loss: 0.1598 - mrcnn_class_loss: 0.1291 - mrcnn_bbox_loss: 0.1812 - mrcnn_mask_loss: 0.13062019-04-09 06:58:45.525489: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:58:45.525594: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice_37. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:58:46.621115: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
2019-04-09 06:58:46.621222: W ./tensorflow/core/grappler/optimizers/graph_optimizer_stage.h:241] Failed to run optimizer ArithmeticOptimizer, stage RemoveStackStridedSliceSameAxis node proposal_targets/strided_slice_37. Error: ValidateStridedSliceOp returned partial shapes [1,?,?] and [?,?]
300/300 [==============================] - 1271s 4s/step - loss: 0.6336 - rpn_class_loss: 0.0346 - rpn_bbox_loss: 0.1593 - mrcnn_class_loss: 0.1287 - mrcnn_bbox_loss: 0.1807 - mrcnn_mask_loss: 0.1303 - val_loss: 0.5545 - val_rpn_class_loss: 0.0277 - val_rpn_bbox_loss: 0.2055 - val_mrcnn_class_loss: 0.1225 - val_mrcnn_bbox_loss: 0.0913 - val_mrcnn_mask_loss: 0.1074
Epoch 2/20
300/300 [==============================] - 1123s 4s/step - loss: 0.2583 - rpn_class_loss: 0.0123 - rpn_bbox_loss: 0.0769 - mrcnn_class_loss: 0.0601 - mrcnn_bbox_loss: 0.0353 - mrcnn_mask_loss: 0.0737 - val_loss: 0.5017 - val_rpn_class_loss: 0.0543 - val_rpn_bbox_loss: 0.1502 - val_mrcnn_class_loss: 0.1191 - val_mrcnn_bbox_loss: 0.0720 - val_mrcnn_mask_loss: 0.1061
Epoch 3/20
300/300 [==============================] - 1123s 4s/step - loss: 0.2058 - rpn_class_loss: 0.0039 - rpn_bbox_loss: 0.0571 - mrcnn_class_loss: 0.0501 - mrcnn_bbox_loss: 0.0288 - mrcnn_mask_loss: 0.0659 - val_loss: 0.6480 - val_rpn_class_loss: 0.1366 - val_rpn_bbox_loss: 0.2171 - val_mrcnn_class_loss: 0.1376 - val_mrcnn_bbox_loss: 0.0676 - val_mrcnn_mask_loss: 0.0890
Epoch 4/20
300/300 [==============================] - 1126s 4s/step - loss: 0.1724 - rpn_class_loss: 0.0070 - rpn_bbox_loss: 0.0447 - mrcnn_class_loss: 0.0398 - mrcnn_bbox_loss: 0.0203 - mrcnn_mask_loss: 0.0605 - val_loss: 0.6016 - val_rpn_class_loss: 0.1308 - val_rpn_bbox_loss: 0.1658 - val_mrcnn_class_loss: 0.1391 - val_mrcnn_bbox_loss: 0.0686 - val_mrcnn_mask_loss: 0.0973
Epoch 5/20
300/300 [==============================] - 1117s 4s/step - loss: 0.1786 - rpn_class_loss: 0.0068 - rpn_bbox_loss: 0.0660 - mrcnn_class_loss: 0.0363 - mrcnn_bbox_loss: 0.0155 - mrcnn_mask_loss: 0.0539 - val_loss: 0.6107 - val_rpn_class_loss: 0.1100 - val_rpn_bbox_loss: 0.1690 - val_mrcnn_class_loss: 0.1659 - val_mrcnn_bbox_loss: 0.0720 - val_mrcnn_mask_loss: 0.0938
Epoch 6/20
300/300 [==============================] - 1124s 4s/step - loss: 0.1514 - rpn_class_loss: 0.0051 - rpn_bbox_loss: 0.0356 - mrcnn_class_loss: 0.0369 - mrcnn_bbox_loss: 0.0186 - mrcnn_mask_loss: 0.0554 - val_loss: 0.5408 - val_rpn_class_loss: 0.1243 - val_rpn_bbox_loss: 0.1535 - val_mrcnn_class_loss: 0.1103 - val_mrcnn_bbox_loss: 0.0514 - val_mrcnn_mask_loss: 0.1013
Epoch 7/20
300/300 [==============================] - 1115s 4s/step - loss: 0.1405 - rpn_class_loss: 0.0029 - rpn_bbox_loss: 0.0349 - mrcnn_class_loss: 0.0375 - mrcnn_bbox_loss: 0.0147 - mrcnn_mask_loss: 0.0505 - val_loss: 0.6750 - val_rpn_class_loss: 0.1701 - val_rpn_bbox_loss: 0.1763 - val_mrcnn_class_loss: 0.1671 - val_mrcnn_bbox_loss: 0.0648 - val_mrcnn_mask_loss: 0.0966
Epoch 8/20
300/300 [==============================] - 1125s 4s/step - loss: 0.1195 - rpn_class_loss: 0.0057 - rpn_bbox_loss: 0.0249 - mrcnn_class_loss: 0.0279 - mrcnn_bbox_loss: 0.0113 - mrcnn_mask_loss: 0.0497 - val_loss: 0.6379 - val_rpn_class_loss: 0.1332 - val_rpn_bbox_loss: 0.1547 - val_mrcnn_class_loss: 0.1620 - val_mrcnn_bbox_loss: 0.0705 - val_mrcnn_mask_loss: 0.1176
Epoch 9/20
300/300 [==============================] - 1118s 4s/step - loss: 0.1179 - rpn_class_loss: 0.0026 - rpn_bbox_loss: 0.0290 - mrcnn_class_loss: 0.0267 - mrcnn_bbox_loss: 0.0105 - mrcnn_mask_loss: 0.0490 - val_loss: 0.7072 - val_rpn_class_loss: 0.1499 - val_rpn_bbox_loss: 0.2015 - val_mrcnn_class_loss: 0.1709 - val_mrcnn_bbox_loss: 0.0707 - val_mrcnn_mask_loss: 0.1143
Epoch 10/20
300/300 [==============================] - 1120s 4s/step - loss: 0.1367 - rpn_class_loss: 0.0093 - rpn_bbox_loss: 0.0370 - mrcnn_class_loss: 0.0295 - mrcnn_bbox_loss: 0.0107 - mrcnn_mask_loss: 0.0502 - val_loss: 0.7095 - val_rpn_class_loss: 0.1799 - val_rpn_bbox_loss: 0.1535 - val_mrcnn_class_loss: 0.1971 - val_mrcnn_bbox_loss: 0.0704 - val_mrcnn_mask_loss: 0.1085
Epoch 11/20
300/300 [==============================] - 1114s 4s/step - loss: 0.1149 - rpn_class_loss: 0.0072 - rpn_bbox_loss: 0.0259 - mrcnn_class_loss: 0.0236 - mrcnn_bbox_loss: 0.0099 - mrcnn_mask_loss: 0.0483 - val_loss: 0.6307 - val_rpn_class_loss: 0.1236 - val_rpn_bbox_loss: 0.1243 - val_mrcnn_class_loss: 0.1919 - val_mrcnn_bbox_loss: 0.0722 - val_mrcnn_mask_loss: 0.1187
Epoch 12/20
300/300 [==============================] - 1118s 4s/step - loss: 0.1210 - rpn_class_loss: 0.0052 - rpn_bbox_loss: 0.0288 - mrcnn_class_loss: 0.0268 - mrcnn_bbox_loss: 0.0114 - mrcnn_mask_loss: 0.0487 - val_loss: 0.6637 - val_rpn_class_loss: 0.1404 - val_rpn_bbox_loss: 0.1543 - val_mrcnn_class_loss: 0.1874 - val_mrcnn_bbox_loss: 0.0703 - val_mrcnn_mask_loss: 0.1113
Epoch 13/20
300/300 [==============================] - 1120s 4s/step - loss: 0.1022 - rpn_class_loss: 0.0035 - rpn_bbox_loss: 0.0204 - mrcnn_class_loss: 0.0224 - mrcnn_bbox_loss: 0.0088 - mrcnn_mask_loss: 0.0471 - val_loss: 0.6452 - val_rpn_class_loss: 0.1505 - val_rpn_bbox_loss: 0.1760 - val_mrcnn_class_loss: 0.1468 - val_mrcnn_bbox_loss: 0.0628 - val_mrcnn_mask_loss: 0.1092
Epoch 14/20
300/300 [==============================] - 1120s 4s/step - loss: 0.1174 - rpn_class_loss: 0.0017 - rpn_bbox_loss: 0.0369 - mrcnn_class_loss: 0.0230 - mrcnn_bbox_loss: 0.0094 - mrcnn_mask_loss: 0.0465 - val_loss: 0.6286 - val_rpn_class_loss: 0.1824 - val_rpn_bbox_loss: 0.1420 - val_mrcnn_class_loss: 0.1438 - val_mrcnn_bbox_loss: 0.0512 - val_mrcnn_mask_loss: 0.1092
Epoch 15/20
300/300 [==============================] - 1119s 4s/step - loss: 0.1150 - rpn_class_loss: 0.0049 - rpn_bbox_loss: 0.0272 - mrcnn_class_loss: 0.0256 - mrcnn_bbox_loss: 0.0092 - mrcnn_mask_loss: 0.0481 - val_loss: 0.7799 - val_rpn_class_loss: 0.1967 - val_rpn_bbox_loss: 0.1656 - val_mrcnn_class_loss: 0.2342 - val_mrcnn_bbox_loss: 0.0670 - val_mrcnn_mask_loss: 0.1164
Epoch 16/20
300/300 [==============================] - 1121s 4s/step - loss: 0.0960 - rpn_class_loss: 0.0024 - rpn_bbox_loss: 0.0257 - mrcnn_class_loss: 0.0189 - mrcnn_bbox_loss: 0.0072 - mrcnn_mask_loss: 0.0418 - val_loss: 0.8424 - val_rpn_class_loss: 0.2791 - val_rpn_bbox_loss: 0.1950 - val_mrcnn_class_loss: 0.1964 - val_mrcnn_bbox_loss: 0.0666 - val_mrcnn_mask_loss: 0.1053
Epoch 17/20
300/300 [==============================] - 1111s 4s/step - loss: 0.1164 - rpn_class_loss: 0.0067 - rpn_bbox_loss: 0.0301 - mrcnn_class_loss: 0.0264 - mrcnn_bbox_loss: 0.0087 - mrcnn_mask_loss: 0.0446 - val_loss: 0.6527 - val_rpn_class_loss: 0.1634 - val_rpn_bbox_loss: 0.1445 - val_mrcnn_class_loss: 0.1681 - val_mrcnn_bbox_loss: 0.0651 - val_mrcnn_mask_loss: 0.1117
Epoch 18/20
300/300 [==============================] - 1115s 4s/step - loss: 0.0929 - rpn_class_loss: 0.0024 - rpn_bbox_loss: 0.0172 - mrcnn_class_loss: 0.0204 - mrcnn_bbox_loss: 0.0081 - mrcnn_mask_loss: 0.0448 - val_loss: 0.5940 - val_rpn_class_loss: 0.1500 - val_rpn_bbox_loss: 0.1414 - val_mrcnn_class_loss: 0.1312 - val_mrcnn_bbox_loss: 0.0654 - val_mrcnn_mask_loss: 0.1060
Epoch 19/20
300/300 [==============================] - 1109s 4s/step - loss: 0.1264 - rpn_class_loss: 0.0144 - rpn_bbox_loss: 0.0412 - mrcnn_class_loss: 0.0197 - mrcnn_bbox_loss: 0.0079 - mrcnn_mask_loss: 0.0433 - val_loss: 0.5891 - val_rpn_class_loss: 0.1212 - val_rpn_bbox_loss: 0.1559 - val_mrcnn_class_loss: 0.1418 - val_mrcnn_bbox_loss: 0.0589 - val_mrcnn_mask_loss: 0.1112
Epoch 20/20
300/300 [==============================] - 1110s 4s/step - loss: 0.1020 - rpn_class_loss: 0.0019 - rpn_bbox_loss: 0.0315 - mrcnn_class_loss: 0.0181 - mrcnn_bbox_loss: 0.0077 - mrcnn_mask_loss: 0.0428 - val_loss: 0.6500 - val_rpn_class_loss: 0.1650 - val_rpn_bbox_loss: 0.1285 - val_mrcnn_class_loss: 0.1781 - val_mrcnn_bbox_loss: 0.0592 - val_mrcnn_mask_loss: 0.1193