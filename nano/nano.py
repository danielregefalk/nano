"""
Usage: import the module (see Jupyter notebooks for examples), or run from the command line as such:

    # Train a new model starting from pre-trained COCO weights
    python balloon.py train --dataset=data --weights=coco

    # Resume training a model that you had trained earlier
    python3 balloon.py train --dataset=/path/to/balloon/dataset --weights=last

    # Train a new model starting from ImageNet weights
    python3 balloon.py train --dataset=/path/to/balloon/dataset --weights=imagenet

    # Apply color splash to an image
    python3 balloon.py splash --weights=/path/to/weights/file.h5 --image=<URL or path to file>

    python balloon.py splash --weights=mask_rcnn_balloon.h5 --image=https://i.ytimg.com/vi/jbTvQ6UYIX8/hqdefault.jpg

    # Apply color splash to video using the last weights you trained
    python3 balloon.py splash --weights=last --video=<URL or path to file>


python nano.py train --dataset=data --weights=coco

python nano.py splash --weights=w30.h5 --image=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT92LAyeLmHqeXrMu3Gc7EjSU0XJO3FaOQmLQLyZ7JZ3yGvyHNp

python nano.py splash --weights=w20x300.h5 --image=https://i.ytimg.com/vi/jbTvQ6UYIX8/hqdefault.jpg
python nano.py splash --weights=w20x300.h5 --image=https://images-na.ssl-images-amazon.com/images/I/31hUt24agTL.jpg
python nano.py splash --weights=w20x300.h5 --image=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRE1KA4-4yyvV-soP6m3_LxXOlpJdOK0QVivswu_uryXySyFXVgf1T-xek
python nano.py splash --weights=w20x300.h5 --image=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT92LAyeLmHqeXrMu3Gc7EjSU0XJO3FaOQmLQLyZ7JZ3yGvyHNp
python nano.py splash --weights=w20x300.h5 --image=https://www.ballongverkstan.se/_files/system-pages-sv-SE-ballongverkstan/79/ballongproducent.jpg?636891444091009676

https://i.ytimg.com/vi/jbTvQ6UYIX8/hqdefault.jpg
https://images-na.ssl-images-amazon.com/images/I/31hUt24agTL.jpg
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRE1KA4-4yyvV-soP6m3_LxXOlpJdOK0QVivswu_uryXySyFXVgf1T-xek
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT92LAyeLmHqeXrMu3Gc7EjSU0XJO3FaOQmLQLyZ7JZ3yGvyHNp
https://www.ballongverkstan.se/_files/system-pages-sv-SE-ballongverkstan/79/ballongproducent.jpg?636891444091009676

"""

import os
import sys
import json
import datetime
import numpy as np
import skimage.draw
from imgaug import augmenters as iaa

#from tensorboardcolab import *


# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

############################################################
#  Configurations
############################################################


class NanoConfig(Config):
    # Give the configuration a recognizable name
    NAME = "nano"

    # Color channels
    IMAGE_CHANNEL_COUNT = 1

    MEAN_PIXEL = (255 / 2)

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + nano

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 300

    # Whether to use image augmentation in training mode
    AUGMENT = True

    # Whether to use image scaling and rotations in training mode
    SCALE = True

    # Optimizer, default is 'SGD'
    OPTIMIZER = 'ADAM'

    # Skip detections with < 90% confidence
    #DETECTION_MIN_CONFIDENCE = 0.9

	# Don't exclude based on confidence. Since we have two classes
    # then 0.5 is the minimum anyway as it picks between nucleus and BG
    DETECTION_MIN_CONFIDENCE = 0

	# Backbone network architecture
    # Supported values are: resnet50, resnet101
    BACKBONE = "resnet101"

	# If enabled, resizes instance masks to a smaller size to reduce
    # memory load. Recommended when using high-resolution images.
    #USE_MINI_MASK = True
    #MINI_MASK_SHAPE = (56, 56)  # (height, width) of the mini-mask

    # ROIs kept after non-maximum supression (training and inference)
    POST_NMS_ROIS_TRAINING = 2000
    POST_NMS_ROIS_INFERENCE = 2000

    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.9

    # How many anchors per image to use for RPN training
    RPN_TRAIN_ANCHORS_PER_IMAGE = 64

    # Max ground truth instances
    MAX_GT_INSTANCES = 50



############################################################
#  Dataset
############################################################

class NanoDataset(utils.Dataset):

    def load_nano(self, dataset_dir, subset):
        """Load a subset of the Balloon dataset.
        dataset_dir: Root directory of the dataset.
        subset: Subset to load: train or val
        """
        # Add classes. We have only one class to add.
        self.add_class("nano", 1, "nano")

        # Train or validation dataset?
        assert subset in ["train", "val"]
        dataset_dir = os.path.join(dataset_dir, subset)

        # Load annotations
        # VGG Image Annotator (up to version 1.6) saves each image in the form:
        # { 'filename': '28503151_5b5b7ec140_b.jpg',
        #   'regions': {
        #       '0': {
        #           'region_attributes': {},
        #           'shape_attributes': {
        #               'all_points_x': [...],
        #               'all_points_y': [...],
        #               'name': 'polygon'}},
        #       ... more regions ...
        #   },
        #   'size': 100202
        # }
        # We mostly care about the x and y coordinates of each region
        # Note: In VIA 2.0, regions was changed from a dict to a list.
        annotations = json.load(open(os.path.join(dataset_dir, "via_region_data.json")))
        annotations = list(annotations.values())  # don't need the dict keys

        # The VIA tool saves images in the JSON even if they don't have any
        # annotations. Skip unannotated images.
        annotations = [a for a in annotations if a['regions']]

        # Add images
        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            # The if condition is needed to support VIA versions 1.x and 2.x.
            if type(a['regions']) is dict:
                polygons = [r['shape_attributes'] for r in a['regions'].values()]
            else:
                polygons = [r['shape_attributes'] for r in a['regions']] 

            # load_mask() needs the image size to convert polygons to masks.
            # Unfortunately, VIA doesn't include it in JSON, so we must read
            # the image. This is only managable since the dataset is tiny.
            image_path = os.path.join(dataset_dir, a['filename'])
            image = skimage.io.imread(image_path)
            height, width = image.shape[:2]

            self.add_image(
                "nano",
                image_id=a['filename'],  # use file name as a unique image id
                path=image_path,
                width=width, height=height,
                polygons=polygons)

    def load_mask(self, image_id):
        """Generate instance masks for an image.
        Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a balloon dataset image, delegate to parent class.
        image_info = self.image_info[image_id]
        if image_info["source"] != "nano":
            return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        info = self.image_info[image_id]
        mask = np.zeros([info["height"], info["width"], len(info["polygons"])],
                        dtype=np.uint8)
        for i, p in enumerate(info["polygons"]):
            # Get indexes of pixels inside the polygon and set them to 1
            rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID only, we return an array of 1s
        return mask.astype(np.bool), np.ones([mask.shape[-1]], dtype=np.int32)

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "nano":
            return info["path"]
        else:
            super(self.__class__, self).image_reference(image_id)


def train(model):
    """Train the model."""
    # Training dataset.
    dataset_train = NanoDataset()
    dataset_train.load_nano(args.dataset, "train")
    dataset_train.prepare()

    # Validation dataset
    dataset_val = NanoDataset()
    dataset_val.load_nano(args.dataset, "val")
    dataset_val.prepare()

    #CUSTOM
    print("Authentication google drive")
    from pydrive.auth import GoogleAuth
    from pydrive.drive import GoogleDrive 
    from google.colab import auth 
    from oauth2client.client import GoogleCredentials

    auth.authenticate_user()
    gauth = GoogleAuth()
    gauth.credentials = GoogleCredentials.get_application_default()
    drive = GoogleDrive(gauth)

    # Image augmentation
    # http://imgaug.readthedocs.io/en/latest/source/augmenters.html
    augmentation = iaa.SomeOf((0, 2), [
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
        iaa.Affine(rotate=(-20, 20)),
        iaa.Crop(percent=0.5),
        iaa.Affine(scale=(0.5, 2))
    ])

    # *** This training schedule is an example. Update to your needs ***
    # Since we're using a very small dataset, and starting from
    # COCO trained weights, we don't need to train too long. Also,
    # no need to train all layers, just the heads should do it.

    EPOCHS = 20

    print("Training network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=EPOCHS,
                layers='all',
                augmentation=augmentation)


    # SAVE MODEL
    for root, dirs, files in os.walk(DEFAULT_LOGS_DIR):
        for file in files:
            if file.endswith(str(EPOCHS) + ".h5"):
                model_file = drive.CreateFile({'title' : os.path.join(root, file)})
                model_file.SetContentFile(os.path.join(root, file))
                model_file.Upload()

def color_splash(image, mask):
    """Apply color splash effect.
    image: RGB image [height, width, 3]
    mask: instance segmentation mask [height, width, instance count]

    Returns result image.
    """
    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
    #zeros = np.zeros(image.shape)
    
    # Copy color pixels from the original color image where mask is set
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        splash = np.where(mask, image, gray).astype(np.uint8)
        #splash = np.where(mask, image, zeros).astype(np.uint8)
    else:
        splash = gray.astype(np.uint8)
    return splash

def detect_and_color_splash(model, image_path):
	assert image_path

	# Run model detection and generate the color splash effect
	print("Running on {}".format(args.image))
	# Read image
	image = skimage.io.imread(args.image)
	# Detect objects
	r = model.detect([image], verbose=1)[0]
	# Color splash
	splash = color_splash(image, r['masks'])
	# Save output
	file_name = "splash_{:%Y%m%dT%H%M%S}.png".format(datetime.datetime.now())
	skimage.io.imsave(file_name, splash)

	print("Saved to ", file_name)


############################################################
#  Training
############################################################

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect balloons.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'splash'")
    parser.add_argument('--dataset', required=False,
                        metavar="/path/to/balloon/dataset/",
                        help='Directory of the Balloon dataset')
    parser.add_argument('--weights', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image to apply the color splash effect on')
    parser.add_argument('--video', required=False,
                        metavar="path or URL to video",
                        help='Video to apply the color splash effect on')
    args = parser.parse_args()

    # Validate arguments
    if args.command == "train":
        assert args.dataset, "Argument --dataset is required for training"
    elif args.command == "splash":
        assert args.image or args.video,\
               "Provide --image or --video to apply color splash"

    print("Weights: ", args.weights)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs)

    # Configurations
    if args.command == "train":
        config = NanoConfig()
    else:
        class InferenceConfig(NanoConfig):
            # Set batch size to 1 since we'll be running inference on
            # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1

            # Don't resize imager for inferencing
            #IMAGE_RESIZE_MODE = "pad64"
            # Non-max suppression threshold to filter RPN proposals.
            # You can increase this during training to generate more propsals.
            RPN_NMS_THRESHOLD = 0.8

        config = InferenceConfig()
    config.display()

    # Create model
    if args.command == "train":
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=args.logs)

    # Select weights file to load
    if args.weights.lower() == "coco":
        weights_path = COCO_WEIGHTS_PATH
        # Download weights file
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    elif args.weights.lower() == "last":
        # Find last trained weights
        weights_path = model.find_last()
    elif args.weights.lower() == "imagenet":
        # Start from ImageNet trained weights
        weights_path = model.get_imagenet_weights()
    else:
        weights_path = args.weights

    # Load weights
    print("Loading weights ", weights_path)
    if args.weights.lower() == "coco":
        # Exclude the last layers because they require a matching
        # number of classes
        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)

    # Train or evaluate
    if args.command == "train":
        train(model)
    elif args.command == "splash":
        detect_and_color_splash(model, image_path=args.image)
    else:
        print("'{}' is not recognized. "
              "Use 'train' or 'splash'".format(args.command))
