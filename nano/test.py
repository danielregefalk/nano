import os
import sys
import math
import random
import numpy as np
import cv2
from scipy.misc import imread, imsave, imresize

from skimage import io

import image_generator

import matplotlib
import matplotlib.pyplot as plt

from PIL import Image

# Root directory of the project
ROOT_DIR = os.path.abspath("../")
# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils, visualize
import mrcnn.model as modellib

class ShapesConfig(Config):
	# Give the configuration a recognizable name
	NAME = "nano"

	# Color channels
	IMAGE_CHANNEL_COUNT = 1
	MEAN_PIXEL = np.array([255 / 2])

	# Train on 1 GPU and 6 images per GPU. We can put multiple images on each
	# GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
	GPU_COUNT = 1
	IMAGES_PER_GPU = 6

	# Number of classes (including background)
	NUM_CLASSES = 1 + 1  # background + 1 shape

	# Backbone network architecture
	# Supported values are: resnet50, resnet101.
	# You can also provide a callable that should have the signature
	# of model.resnet_graph. If you do so, you need to supply a callable
	# to COMPUTE_BACKBONE_SHAPE as well
	BACKBONE = "resnet101"

	# Non-max suppression threshold to filter RPN proposals.
	# You can increase this during training to generate more propsals.
	RPN_NMS_THRESHOLD = 0.9

	# Input image resizing
	# Random crops of size 512x512
	IMAGE_MIN_DIM = 512
	IMAGE_MAX_DIM = 512
	#IMAGE_RESIZE_MODE = "crop"
	#IMAGE_MIN_SCALE = 2.0

	# Length of square anchor side in pixels
	# Use smaller anchors because our image and objects are small
	RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)

	# Reduce training ROIs per image because the images are small and have
	# few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
	TRAIN_ROIS_PER_IMAGE = 256

	# Train and validation steps
	STEPS_PER_EPOCH = 200
	VALIDATION_STEPS = 20

	# Learning rate and momentum
	# The Mask RCNN paper uses lr=0.02, but on TensorFlow it causes
	# weights to explode. Likely due to differences in optimizer
	# implementation.
	LEARNING_RATE = 0.001
	LEARNING_MOMENTUM = 0.9

class ShapesDataset(utils.Dataset):

	def load_shapes(self, count, height, width, prefix):
		self.add_class("nano", 1, "nano")

		print("Starting to generate data")

		for i in range(count):
			if (i % 10 == 0 ):
				print(str(i) if i % 100 == 0 else ".", end=" ", flush=True)
			image_generator.create_image((prefix + str(i)), width, height)

			self.add_image("nano", image_id=(prefix + str(i)), path=None, width=width, height=height)

		print(" ")
		print("Done generating data")

	def load_image(self, index):
		info = self.image_info[index]
		#image = io.imread('train/' + str(info['id']) + '.png')
		image = Image.open('train/' + str(info['id']) + '.png').convert('L')
		image = np.array(image)
		#print(image.shape)
		image = image[..., np.newaxis]
		#print(image.shape)


		#print("shapez", image)
		#print("shapez", image.shape)

		return image

	def load_mask(self, index):
		info = self.image_info[index]
		masks = np.load('train/' + str(info['id']) + 'masks.npy')

		return masks, np.ones([masks.shape[-1]], dtype=np.int32)

# Config
config = ShapesConfig()
config.display()

# Training dataset
dataset_train = ShapesDataset()
dataset_train.load_shapes(2000, config.IMAGE_SHAPE[0], config.IMAGE_SHAPE[1], "train")
dataset_train.prepare()

# Validation dataset
dataset_val = ShapesDataset()
dataset_val.load_shapes(200, config.IMAGE_SHAPE[0], config.IMAGE_SHAPE[1], "validate")
dataset_val.prepare()

# Load and display random samples
if False:
	image_ids = np.random.choice(dataset_train.image_ids, 4)
	for image_id in image_ids:
		image = dataset_train.load_image(image_id)
		mask, class_ids = dataset_train.load_mask(image_id)
		visualize.display_top_masks(image, mask, class_ids, dataset_train.class_names)

# Create model in training mode
#print("BEFORE MODEL")
model = modellib.MaskRCNN(mode="training", config=config,
							model_dir=MODEL_DIR)
#print("BEFORE AFTER")

# SET WEIGHTS
if True:
	# Load COCO weights
	
	COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
	# Download COCO trained weights from Releases if needed
	if not os.path.exists(COCO_MODEL_PATH):
		utils.download_trained_weights(COCO_MODEL_PATH)


	model.load_weights(COCO_MODEL_PATH, by_name=True,
						exclude=["mrcnn_class_logits", "mrcnn_bbox_fc", 
								"mrcnn_bbox", "mrcnn_mask", "conv1"])


	#model.load_weights(weights_path, by_name=True,exclude=[   "conv1"])             

if False:
	# Load the last model you trained and continue training
	model.load_weights(model.find_last(), by_name=True)

model.train(dataset_train, dataset_val, 
			learning_rate=config.LEARNING_RATE, 
			epochs=30, 
			layers='all') #'heads' OR 'all'
