import os
import sys
import math
import random
import numpy as np
import cv2
from PIL import Image, ImageEnhance, ImageFilter

from scipy.misc import imread, imsave, imresize
import matplotlib.pyplot as plt

import imgaug as ia
from imgaug import augmenters as iaa

def create_image(index, width, height):
	bg = Image.open('img/noise.png')#.convert('L')
	bg = bg.resize((width, height), resample=Image.BICUBIC)
	#bg = Image.new('RGB', (width, height), color=0)
	mask = Image.new('L', (width, height), color=0)

	# Varying BG
	seq = iaa.Sequential([
		iaa.Fliplr(0.5),
		iaa.Sometimes(0.5,
			iaa.GaussianBlur(sigma=(0, 0.5))
		),
		iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
		iaa.Affine(
			shear=(-1, 1),
			rotate=(-90, 90)
		)
	], random_order=True)

	bg = Image.fromarray(seq.augment_images([np.array(bg)])[0])

	NUM_ARTIFACTS = random.randint(0, 5)
	NUM_OBJECTS = random.randint(0, 20)
	masks = []

	# ADD NOISE
	for i in range(NUM_ARTIFACTS):
		file_path = 'img/artifacts/a' + str(random.randint(1, 7)) + '.png'
		nano = Image.open(file_path)

		#converter = ImageEnhance.Brightness(a)
		#a = converter.enhance(0.5 + random.random() * 1.0)


		# AUGMENT
		seq = iaa.Sequential([
			iaa.Fliplr(0.5),
			#iaa.Sometimes(0.5,
			iaa.GaussianBlur(sigma=(0, 1)),
			#),
			iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
			#iaa.Multiply((0.8, 1.2), per_channel=0.2),
			iaa.Affine(
				shear=(-5, 5)
			)
		], random_order=True)

		nano = Image.fromarray(seq.augment_images([np.array(nano)])[0])

		rotation = random.random() * 360
		scale_x = random.random() * 3 * 0.4 + 0.2
		scale_y = (random.random() * 0.2 + 0.9) * scale_x
		new_size = (int(nano.size[0] * scale_x), int(nano.size[1] * scale_y))
		position = (random.randint(0, width), random.randint(0, height))

		nano = nano.rotate(rotation, resample=Image.BICUBIC, expand=True)
		nano = nano.resize(new_size, resample=Image.BICUBIC)

		# ADD TO IMAGE
		bg.paste(nano, position, nano)

	# ADD NANOS
	for i in range(NUM_OBJECTS):
		file_path = 'img/nanos/nano' + str(random.randint(1, 12)) + '.png'
		nano = Image.open(file_path)

		converter = ImageEnhance.Brightness(nano)
		nano = converter.enhance(0.5 + random.random() * 1.0)


		# AUGMENT
		seq = iaa.Sequential([
			iaa.Fliplr(0.5),
			iaa.Sometimes(0.5,
				iaa.GaussianBlur(sigma=(0, 0.5))
			),
			iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
			#iaa.Multiply((0.8, 1.2), per_channel=0.2),
			iaa.Affine(
				shear=(-1, 1)
			)
		], random_order=True)

		nano = Image.fromarray(seq.augment_images([np.array(nano)])[0])

		rotation = random.random() * 4 - 2
		scale_x = random.random() * 0.4 + 0.2
		scale_y = (random.random() * 0.2 + 0.9) * scale_x
		new_size = (int(nano.size[0] * scale_x), int(nano.size[1] * scale_y))
		position = (random.randint(0, width), random.randint(0, height))

		nano = nano.rotate(rotation, resample=Image.BICUBIC, expand=True)
		nano = nano.resize(new_size, resample=Image.BICUBIC)

		# ADD TO IMAGE
		bg.paste(nano, position, nano)

		alpha_channel = nano.split()[-1]

		# SAVE MASK
		nano_mask = Image.new('L', (width, height), color=0)
		nano_mask.paste(255, position, mask=nano)

		if nano_mask.getbbox():
			mask_array = np.array(nano_mask)
			masks.append(mask_array)

	num_masks = len(masks)

	if num_masks == 0:
		print("No mask => skip")
		masks = np.empty((width, height, 0))
	
	else:
		masks = np.stack(masks, axis=-1)
		masks[masks >= 20] = 255
		masks[masks < 20] = 0

		# Handle occlusions
		occlusion = np.logical_not(masks[:, :, -1]).astype(np.uint8)
		for i in range(num_masks - 2, -1, -1):
			masks[:, :, i] = masks[:, :, i] * occlusion
			occlusion = np.logical_and(
				occlusion, np.logical_not(masks[:, :, i]))

		# Show masks
		for i in range(num_masks):
			masken = Image.fromarray(masks[:, :, i])
			masken.show()

	# Show image
	bg.show()

	#bg.save('train/' + str(index) + '.png')
	#np.save('train/' + str(index) + 'masks', masks)

create_image('test', 512, 512)