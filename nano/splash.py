import os
import sys
import math
import random
import numpy as np
import cv2
from scipy.misc import imread, imsave, imresize
import skimage.draw
import datetime

import matplotlib
import matplotlib.pyplot as plt

from PIL import Image

# Root directory of the project
ROOT_DIR = os.path.abspath("../")
# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils, visualize
import mrcnn.model as modellib



def color_splash(image, mask):
	print("splash:", image.shape, mask.shape)

	# OLD
	#img = Image.new('RGB', (mask.shape[0], mask.shape[1]), color = 'blue')
	#gray = np.array(img)


	gray_image = np.squeeze(image, axis=2)
	gray = skimage.color.gray2rgb(gray_image)
	#blue = gray * [1, 0, 1]
	
	splash = gray.astype(np.uint8)

	# Copy color pixels from the original color image where mask is set
	#print(mask.shape)
	for i in range(mask.shape[-1]):
		instance_mask = mask[:,:,i]
		instance_mask = np.expand_dims(instance_mask, axis=2)
		
		color = gray * [random.random(), random.random(), random.random()]	

		splash = np.where(instance_mask, color, splash).astype(np.uint8)

	"""
	if mask.shape[-1] > 0:
		# We're treating all instances as one, so collapse the mask into one layer
		mask = (np.sum(mask, -1, keepdims=True) >= 1)

		color = gray * [random.random(), random.random(), random.random()]	
		splash = np.where(mask, color, image).astype(np.uint8)
		#splash = np.where(mask, image, zeros).astype(np.uint8)
	else:
		splash = gray.astype(np.uint8)
	"""

	return splash

def detect_and_color_splash(model):
	#assert image_path

	# Run model detection and generate the color splash effect
	#print("Running on {}".format(image_path))
	# Read image
	#image = skimage.io.imread(image_path)

	# Detect objects
	#"""
	NUM_TESTS = 9
	images = []
	for i in range(NUM_TESTS):
		#test_file = "test" + str(i)
		#image_generator.create_image(test_file, 512, 512)
		#image = Image.open("test" + str(i) + ".png").convert('L')
		if i == 0: 
			image = Image.open("test" + str(i) + ".png").convert('L')
		else:
			image = Image.open("test" + str(i) + ".tif").convert('L')

		image = np.array(image)
		image = image[..., np.newaxis]
		images.append(image)

		# Detect
		r = model.detect([image], verbose=0)[0]

		# Color splash
		splash = color_splash(image, r['masks'])
		splash_image = Image.fromarray(splash)
		splash_image.show()

	results = model.detect([image], verbose=0)

	for i in range(NUM_TESTS):
		image = images[i]
		result = results[i]

		splash = color_splash(image, result['masks'])
	
		splash_image = Image.fromarray(splash)
		splash_image.show()


############################################################
#  Training
############################################################

import image_generator

if __name__ == '__main__':

	WEIGHTS_PATH = "mask_rcnn_nano_0020.h5"

	class InferenceConfig(Config):
		
		# Give the configuration a recognizable name
		NAME = "nano"

		# Color channels
		IMAGE_CHANNEL_COUNT = 1
		MEAN_PIXEL = np.array([255 / 2])

		# Train on 1 GPU and 8 images per GPU. We can put multiple images on each
		# GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
		GPU_COUNT = 1
		IMAGES_PER_GPU = 1

		# Number of classes (including background)
		NUM_CLASSES = 1 + 1  # background + nano


		BACKBONE = "resnet101"

		# Use small images for faster training. Set the limits of the small side
		# the large side, and that determines the image shape.
		IMAGE_MIN_DIM = 512
		IMAGE_MAX_DIM = 512

		# Use smaller anchors because our image and objects are small
		#RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)  # anchor side in pixels
		RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)

		# Reduce training ROIs per image because the images are small and have
		# few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
		TRAIN_ROIS_PER_IMAGE = 256

		
		# Don't resize imager for inferencing
		#IMAGE_RESIZE_MODE = "pad64"
		# Non-max suppression threshold to filter RPN proposals.
		# You can increase this during training to generate more propsals.
		RPN_NMS_THRESHOLD = 0.9

		# Max number of final detections
		DETECTION_MAX_INSTANCES = 400



	# Root directory of the project
	ROOT_DIR = os.path.abspath("../")
	# Directory to save logs and trained model
	MODEL_DIR = os.path.join(ROOT_DIR, "logs")


	config = InferenceConfig()

	model = modellib.MaskRCNN(mode="inference", config=config,
							model_dir=MODEL_DIR)

	model.load_weights(WEIGHTS_PATH, by_name=True)

	detect_and_color_splash(model)
